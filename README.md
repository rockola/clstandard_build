Common Lisp standard
====================

(Original version by Currell Berry 2017-07-31)

This folder contains the TeX sources of the final Common Lisp standard
draft, along with some scripts written to handle
compiling them into a PDF with a sidebar index (for the output of this
process, please see
[cl-ansi-standard-draft-w-sidebar.pdf](PDF/cl-ansi-standard-draft-w-sidebar.pdf).

The instructions immediately below document how to build the PDF with
sidebar links.  To build an older revision of the draft, which is a
simple concatenation of a number of individual pdfs, see the file
[old_instructions.md](old_instructions.md).

## Dependencies

- Linux
- A Latex distribution
- Make

## Build

```
./maker.sh build && ./maker.sh solidify
```
