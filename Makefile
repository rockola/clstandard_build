TARGET=Common_Lisp_Standard
OUTPUT=build

${OUTPUT}/${TARGET}.pdf:
	@mkdir -p ${OUTPUT}
	cd TeX; pdftex --jobname="${TARGET}" --output-directory=../"${OUTPUT}" includer.tex

